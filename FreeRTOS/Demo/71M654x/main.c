/*
	FreeRTOS.org V5.0.4 - Copyright (C) 2003-2008 Richard Barry.

	This file is part of the FreeRTOS.org distribution.

	FreeRTOS.org is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	FreeRTOS.org is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with FreeRTOS.org; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	A special exception to the GPL can be applied should you wish to distribute
	a combined work that includes FreeRTOS.org, without being obliged to provide
	the source code for any proprietary components.  See the licensing section 
	of http://www.FreeRTOS.org for full details of how and when the exception
	can be applied.

    ***************************************************************************
    ***************************************************************************
    *                                                                         *
    * SAVE TIME AND MONEY!  We can port FreeRTOS.org to your own hardware,    *
    * and even write all or part of your application on your behalf.          *
    * See http://www.OpenRTOS.com for details of the services we provide to   *
    * expedite your project.                                                  *
    *                                                                         *
    ***************************************************************************
    ***************************************************************************

	Please ensure to read the configuration and relevant port sections of the
	online documentation.

	http://www.FreeRTOS.org - Documentation, latest information, license and 
	contact details.

	http://www.SafeRTOS.com - A version that is certified for use in safety 
	critical systems.

	http://www.OpenRTOS.com - Commercial support, development, porting, 
	licensing and training services.
*/

/*
 * Creates the demo application tasks, then starts the scheduler.  The WEB
 * documentation provides more details of the demo application tasks.
 * 
 * Main. c also creates four other tasks:
 * 
 */
 
#include <string.h> 
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "CE/ce.h"

#define mainLOW_PRIORITY		(tskIDLE_PRIORITY+1)
#define mainMIDDLE_PRIORITY		(tskIDLE_PRIORITY+2)

static void prvSetupHardware( void );
static void fRandom0(void *pvParameters);
static void fRandom1(void *pvParameters);
static void fRandom2(void *pvParameters);
static void fRandom3(void *pvParameters);
static void fRandom4(void *pvParameters);
static void fRandom5(void *pvParameters);
static void fRandom6(void *pvParameters);
static void fRandom7(void *pvParameters);
static void fRandom8(void *pvParameters);
static void fRandom9(void *pvParameters);
static void Sensor0(void *pvParameters);

#define RESET_WD()  (WDRST = 0x80)

static volatile xSemaphoreHandle 	xSemaphoreSensor0;
static volatile xTaskHandle			xTaskSensor0;
static xQueueHandle 				xQueueSensor0; 

static portCHAR seed0 = 0x01;
static portCHAR seed1 = 0x02;
static portCHAR seed2 = 0x03;
static portCHAR seed3 = 0x04;
static portCHAR seed4 = 0x05;
static portCHAR seed5 = 0x06;
static portCHAR seed6 = 0x07;
static portCHAR seed7 = 0x08;
static portCHAR seed8 = 0x09;
static portCHAR seed9 = 0x0A;

/*
 * Starts all the other tasks, then starts the scheduler. 
 */
void main( void )
{    
	portBASE_TYPE xReturn = pdPASS;
	
  //Desabilitar o Watchdog: Colocar o pino ICE_E para 1.
	prvSetupHardware();	
	
	//Inicializa Heap
	//vPortInitialiseBlocks();
	
	//Cria Tarefa
	xReturn |= xTaskCreate(fRandom0, "R0", configMINIMAL_STACK_SIZE, &seed0, mainLOW_PRIORITY, ( xTaskHandle * ) NULL );
  xReturn |= xTaskCreate(fRandom1, "R1", configMINIMAL_STACK_SIZE, &seed1, mainLOW_PRIORITY, ( xTaskHandle * ) NULL );
  xReturn |= xTaskCreate(fRandom2, "R2", configMINIMAL_STACK_SIZE, &seed2, mainLOW_PRIORITY, ( xTaskHandle * ) NULL );
  xReturn |= xTaskCreate(fRandom3, "R3", configMINIMAL_STACK_SIZE, &seed3, mainLOW_PRIORITY, ( xTaskHandle * ) NULL );
  xReturn |= xTaskCreate(fRandom4, "R4", configMINIMAL_STACK_SIZE, &seed4, mainLOW_PRIORITY, ( xTaskHandle * ) NULL );
  xReturn |= xTaskCreate(fRandom5, "R5", configMINIMAL_STACK_SIZE, &seed5, mainLOW_PRIORITY, ( xTaskHandle * ) NULL );
  xReturn |= xTaskCreate(fRandom6, "R6", configMINIMAL_STACK_SIZE, &seed6, mainLOW_PRIORITY, ( xTaskHandle * ) NULL );
  xReturn |= xTaskCreate(fRandom7, "R7", configMINIMAL_STACK_SIZE, &seed7, mainLOW_PRIORITY, ( xTaskHandle * ) NULL );
  xReturn |= xTaskCreate(fRandom8, "R8", configMINIMAL_STACK_SIZE, &seed8, mainLOW_PRIORITY, ( xTaskHandle * ) NULL );
  xReturn |= xTaskCreate(fRandom9, "R9", configMINIMAL_STACK_SIZE, &seed9, mainLOW_PRIORITY, ( xTaskHandle * ) NULL );
  xReturn |= xTaskCreate(Sensor0, "S0" , configMINIMAL_STACK_SIZE, &secLowIntegrity, mainMIDDLE_PRIORITY, &xTaskSensor0 );
	vSemaphoreCreateBinary( xSemaphoreSensor0 );
	xQueueSensor0 = xQueueCreate( 1, ( unsigned portBASE_TYPE ) sizeof( portCHAR ) );

	if( xReturn == pdPASS 
	      && xSemaphoreSensor0 != NULL
	      && xQueueSensor0 	   != NULL){
		/* Finally kick off the scheduler.  This function should never return. */
		vTaskStartScheduler();
	}else{
		ERROR = 0x01;
	}

	while(1)
	{
	}	

	/* Should never reach here as the tasks will now be executing under control
	of the scheduler. */  
  
}
/*-----------------------------------------------------------*/

/*
 * Configuração do 71M6543 incluíndo cópia de dados do CE e configuração
 * completa do FE para um medidor trifásico incluindo o neutro.
 */
static void prvSetupHardware( void )
{
	//Desliga interrupções
	EA = 0;
	
  // Set up the MPU.
  CKCON       = 0x00; // The static RAM is fast; even shared. Best performance (p. 35 datasshet)
  PDATA       = 0x02; // Set up the PDATA upper address byte. ADRMSB
  DPL         = 0x00;
  DPH         = 0x00;
  DPL1        = 0x00;
  DPH1        = 0x00;
  DPS         = 0x00; // Select data pointer 0.
  
  // Clear all existing interrupts
  IEN0        = 0x00;
  IEN1        = 0x00;
  IEN2        = 0x00;  
  
  // Clear all waiting interrupts.
  IRCON       = 0x00;

  // Reset the interrupt priorities.
  IP0         = 0x0C;
  IP1         = 0x1B;
  // Interrupt priorities: frequent interrupts are higher.
  // Group,               Level
  // 0=int0, ser1         01 (serial, timer, pulse: 100Hz-~1Kz)
  // 1=timer0,ce_pulses   01
  // 2=int1, ce_busyz     10 (ce busyZ, ~2KHz)
  // 3=timer1, vstat      11 (VSTAT= no power, so no time)
  // 4=ser0, eeprom/SPI   01 (serial... 1KHz)
  // 5=xfer_busy,rtc1s    00 (1Hz..)  

  // Set up the digital I/O ports.  
  // Zero grounds the pins to help prevent oscillation and EMI.
  // DIO 0 & 1 (PORT0 bit 0 & 1) are Wh and VARh pulses, outputs.  (high off)
  // DIO 2 & 3 are the EEPROM pins, outputs. High so EEPROM is not selected.
  // DIO 4 is for the bit-banged RS232 input.
  // DIO 5 reads the button on the debug PCB. It should be an input.
  // DIO 5 is also TX2; On a serial output, idle is high.
  // DIO 6 is XPULSE, overridden by the I/O RAM, output. (high off)
  // DIO 7 is YPULSE, overridden by the I/O RAM, output. (high off)
  // DIO 9 is for the bit-banged RS-232 output.  Idle is high.
  // DIO 52 and 53 drive D2 and D3 on the debug PCB.
  // All others should default to inputs, so they don't drive the LCDs.
  //P0       = 0xF0;     //Acende os LEDs
  //P1       = 0xCC;
  //P2       = 0x20;
  //P3       = 0x00;
  /*P0       = 0xF3;
  P1       = 0xCC;
  P2       = 0x20;
  P3       = 0x00;

  T2CON    = 0x60;     // Interrupt when CE_BUSY & PULSE go high. //TODO tratar essa interrupção
  PSW      = 0x00;
  A        = 0x00;
  B        = 0x00;

  // Clear all the interrupt flags
  FLAG0 = 0;
  FLAG1 = 0;    
  
  //Configura I/O Map
  // This is a 3-phase meter with neutral current.  
  // MUX_DIV is off at first! Changing the selectors with the multiplexor
  //   running can cause contention. 
  // Order of multiplexer is:
  //   IA, VA, IB, VB, IC, VC, IN, unused, unused, unused, unused
  //   The unused ADC slots are set to an invalid number (1) as a defensive
  //   programming practice.  1 is invalid because IA is differential,
  //   so current input 1 is IAN, thus not selectable.
  //   Remote current sensors need a different sequence:
  //   The voltages are clustered at the end of the cycle.
  //   The ADC slots at the start of the cycle should be unused, invalid
  //   ADC slot numbers (e.g. "1" if IA is differential.)
  // The equation is "5" so the CE code makes Wh = VA*IA + VB*IB + VC*IC
  // The Vref chop is automatic (the best.)
  // RTM_E is off, because we don't need ADC data out of the IC. (RTM is
  //   a bit-serial data stream from TMUX1's 0x1F selection. It sends
  //   DSP data.)
  // CE_E (the CE) is off because its input system is not yet configured.
  // SUMSAMPS is set to 0x0888, 2184 decimal, the number of mux
  //   frames per summation interval. It makes the  summation interval
  //   ~1 S. Why? ADC's FIR length = 288, 2 crystal times * 7 samples 
  //   per mux frame, and one  crystal time of silence.
  //   (2*7)+1 = (15/32768S)/sample.  So, the Fs (sample frequency) = 
  //   32768/15 = 2184.53Hz
  // The CE's code is at 0x03*0x400, 0xC00 because the Keil linker
  //   is commanded to put it there in the build options.
  // The pulse width is 0.01S, very good for most calibrators.
  // The current inputs are all differential.
  // RTM is set to the first 4 ADC locations, so if it is enabled, good
  //   data comes out.
  MUX5    = 0x01;
  MUX4    = 0x11;
  MUX3    = 0x10; 
  MUX2    = 0xA6; 
  MUX1    = 0x94; 
  MUX0    = 0x82; 
  CE6     = 0xA0; //Para desligar o CE, CE6 |= 0xEE;
  CE5     = 0x08;
  CE4     = 0x88;
  CE3     = 0x39; // 0x39 . 0x400 = 0xE400 (Endereço de incio do CE, variável CeCode) originalmente 0x03;
  CE2     = 0x42; 
  CE1     = 0x5D; 
  CE0     = 0xF2; 
  RTM0_H  = 0x00; 
  RTM0    = 0x00; 
  RTM1    = 0x01;
  RTM2    = 0x02;
  RTM3    = 0x03;
  //RTM4    = 0x04; No software exemplo é setado o endereço 0x2112 para 0x04, porém esse endereço é inválido
  
  CKGN    = 0x10; //Clock tree: MCK=19.6608MHz, ADC,MPU=4.9152MHz
  
  MUX5    = 0x71; //set mux_div after ADC clock. Read 7 voltages per mux frame.
  
  // Don't attach any interrupts or timer gates to pins.
  // Set the EEPROM interface to I2C (not needed by the CE)
  // Set the optical pin to send a Wh pulse.
  // The optical pin is not inverted. (i.e. negative-going pulse, for an LED)
  // The pulse outputs are disabled to prevent bad pulses
  //     from the CE start-up.  
  // External interrupts
  DIO_R5  = 0x00;
  DIO_R4  = 0x00;
  DIO_R3  = 0x00;
  DIO_R2  = 0x00;
  DIO_R1  = 0x00;
  DIO_R0  = 0x00;
  DIO0    = 0x48;
  DIO1    = 0x00;
  DIO2    = 0x00;
  
  // CE interrupts are enabled, others disabled.
  // The flash controls are zeroed (not needed by the CE).
  // Vref is placed on a pin so it can be measured easily. (VREF_CAL = 1)
  //   Very helpful for debugging the temperature compensation of Vref.  
  //   Not recommended for production meters.
  // The preamp is disabled (PRE_E = 0)
  // The ADC is enabled (ADC_E = 1)
  // The battery-measurement current source is off. (not needed by the CE)
  INT1_E  = 0x61; // 2700, No preamp, Ch. 1
  INT2_E  = 0x00;
  SECURE  = 0x00;
  Analog0 = 0x00;
  VERSION = 0x90;
  
  // Remote current sensors are disabled.
  // DIO outputs are enabled (PORT_E = 1)
  // Clock is sent to both the SPI and EEPROM (SPI_E = 1; for realism)
  // Slave SPI is prevented from corrupt RAM, especially CE RAM (SPI_SAFE = 1)
  RCE0    = 0x85; // 2709; Remotes disabled
  RTMUX   = 0x55;
  //No software exemplo é setado o endereço 0x270B para 0x00, porém esse endereço é inválido
  DIO3    = 0x38;
  */
  //Copia CE data salvo em flash para o início da RAM
  memcpy(&CE_RESERVED, CeData, NumCeData*4);
  
  //Configura interrupções externas (Sensores tampers)
  EX0 = 1; //Para disparar a interrupção setar IE0 (sfr 0x88[1] | TCON.1) set bit TCON.1 1
}

/*-----------------------------------------------------------*/
static void fRandom0(void *pvParameters){	
  TESTE0 = *(unsigned portCHAR*)pvParameters;

  for(;;){
    TESTE0 ^= (TESTE0 << 7);
    TESTE0 ^= (TESTE0 >> 5);
    TESTE0 ^= (TESTE0 << 3);				
  }
}

/*-----------------------------------------------------------*/
static void fRandom1(void *pvParameters){	
  TESTE1 = *(unsigned portCHAR*)pvParameters;

  for(;;){
    TESTE1 ^= (TESTE1 << 7);
    TESTE1 ^= (TESTE1 >> 5);
    TESTE1 ^= (TESTE1 << 3);				
  }
}

/*-----------------------------------------------------------*/
static void fRandom2(void *pvParameters){	
  TESTE2 = *(unsigned portCHAR*)pvParameters;

  for(;;){
    TESTE2 ^= (TESTE2 << 7);
    TESTE2 ^= (TESTE2 >> 5);
    TESTE2 ^= (TESTE2 << 3);				
  }
}

/*-----------------------------------------------------------*/
static void fRandom3(void *pvParameters){	
  TESTE3 = *(unsigned portCHAR*)pvParameters;

  for(;;){
    TESTE3 ^= (TESTE3 << 7);
    TESTE3 ^= (TESTE3 >> 5);
    TESTE3 ^= (TESTE3 << 3);				
  }
}

/*-----------------------------------------------------------*/
static void fRandom4(void *pvParameters){	
  TESTE4 = *(unsigned portCHAR*)pvParameters;

  for(;;){
    TESTE4 ^= (TESTE4 << 7);
    TESTE4 ^= (TESTE4 >> 5);
    TESTE4 ^= (TESTE4 << 3);				
  }
}

/*-----------------------------------------------------------*/
static void fRandom5(void *pvParameters){	
  TESTE5 = *(unsigned portCHAR*)pvParameters;

  for(;;){
    TESTE5 ^= (TESTE5 << 7);
    TESTE5 ^= (TESTE5 >> 5);
    TESTE5 ^= (TESTE5 << 3);				
  }
}

/*-----------------------------------------------------------*/
static void fRandom6(void *pvParameters){	
  TESTE6 = *(unsigned portCHAR*)pvParameters;

  for(;;){
    TESTE6 ^= (TESTE6 << 7);
    TESTE6 ^= (TESTE6 >> 5);
    TESTE6 ^= (TESTE6 << 3);				
  }
}

/*-----------------------------------------------------------*/
static void fRandom7(void *pvParameters){	
  TESTE7 = *(unsigned portCHAR*)pvParameters;

  for(;;){
    TESTE7 ^= (TESTE7 << 7);
    TESTE7 ^= (TESTE7 >> 5);
    TESTE7 ^= (TESTE7 << 3);				
  }
}

/*-----------------------------------------------------------*/
static void fRandom8(void *pvParameters){	
  TESTE8 = *(unsigned portCHAR*)pvParameters;

  for(;;){
    TESTE8 ^= (TESTE8 << 7);
    TESTE8 ^= (TESTE8 >> 5);
    TESTE8 ^= (TESTE8 << 3);				
  }
}

/*-----------------------------------------------------------*/
static void fRandom9(void *pvParameters){	
  TESTE9 = *(unsigned portCHAR*)pvParameters;

  for(;;){
    TESTE9 ^= (TESTE9 << 7);
    TESTE9 ^= (TESTE9 >> 5);
    TESTE9 ^= (TESTE9 << 3);				
  }
}

void vExInt0ISR( void ) __interrupt 0
{
	/* 8051 port interrupt routines MUST be placed within a critical section
	if taskYIELD() is used within the ISR! */	
	static portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	portCHAR pOwner;
	portENTER_CRITICAL();
	{		 
		if ( xTaskResumeFromISR(xTaskSensor0) == pdTRUE){
			pOwner = 1;//secLowIntegrity;
			if (xQueueSendFromISR( xQueueSensor0, &pOwner, &xHigherPriorityTaskWoken ) == pdTRUE){
				//Como tarefa de maior prioridade, solicita troca de contexto
				if ( xHigherPriorityTaskWoken == pdTRUE ){
					portYIELD();
				}else{
					//STEP = 0xAA; //Não representa erro, apenas que a tarefa que foi solicitada
								       // para inicar não tem prioridade mais alta
				}
			}
			else{
				ERROR = 0xAC; //Tentou adicionar mais um item na fila cheia...
			}	
		}else
			ERROR = 0xAB;
	}
	portEXIT_CRITICAL();
}

static void Sensor0(void *pvParameters)
{
  void *pOwner;
	//static volatile portCHAR *buffer;
  __xdata static portSTACK_TYPE * __data buffer;
	//( void ) pvParameters;	
  
  
  buffer = NULL;
	while(1){
		vTaskSuspend( NULL );
    //STEP = 0x00;
		// If the semaphore is not available
		// wait 1 ticks to see if it becomes free.	
		if( xQueueReceive( xQueueSensor0, &pOwner, portMAX_DELAY ) == pdTRUE )
		{
			// It is time to execute.
			//RFLAG += 1;
      //if (getMemory(pvParameters, 0x3400/*(portSHORT)&RFLAG*/, 1, buffer) == pdPASS)
      //STEP = 0x01;
      buffer = getMemory(pvParameters, (portSHORT)&RFLAG/*(portSHORT)&RFLAG*/, 1);
      //STEP = 0xFF;
      if ( buffer != NULL )
      {
        VAR1 = (portSHORT)buffer;
        VAR2 = *buffer;
        (*buffer)++;
        //STEP = 0xb0;
        if ( setMemory(pvParameters, 0x3400/*(portLONG)&RFLAG*/, 1, buffer) > 0)
        {
          //STEP = 0xb1;
          vPortFree(buffer);
        }else
        {
          //STEP = 0xB2;
          ERROR = 0xE1;
        }
      }else
      {  
        ERROR = 0xE0;
      }
		}
		else
		{
			ERROR = 0xAD; //Erro ao obter valor na fila
		}
    //STEP = 0xFF;
	}
}
