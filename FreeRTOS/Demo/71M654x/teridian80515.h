/*-------------------------------------------------------------------------
  Register Declarations for SIEMENS/INFINEON SAB 80515 Processor

   Written By - Bela Torok
   Bela.Torokt@kssg.ch
   based on reg51.h by Sandeep Dutta sandeep.dutta@usa.net
   KEIL C compatible definitions are included

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

   In other words, you are welcome to use, share and improve this program.
   You are forbidden to forbid anyone else to use, share and improve
   what you give them.   Help stamp out software-hoarding!
    
   File port to Silergy 71M6543. Under development.
   Port by Alex Xavier alex.xavier@pucpr.br
-------------------------------------------------------------------------*/

#ifndef TERIDIAN_80515_H
#define TERIDIAN_80515_H

/* BYTE addressable registers */
__sfr __at 0x80 P0          ;
__sfr __at 0x81 SP          ;
__sfr __at 0x82 DPL         ;
__sfr __at 0x83 DPH         ;
__sfr __at 0x84 DPL1        ; /* Teridian implementa um data pointer duplo 
                                no mesmo endereco do SFRPAGE no C8051F120*/
__sfr __at 0x85 DPH1        ;
__sfr __at 0x87 PCON        ;
__sfr __at 0x88 TCON        ;
__sfr __at 0x89 TMOD        ;
__sfr __at 0x8A TL0         ;
__sfr __at 0x8B TL1         ;
__sfr __at 0x8C TH0         ;
__sfr __at 0x8D TH1         ;
__sfr __at 0x8E CKCON       ;
__sfr __at 0x90 P1          ;
__sfr __at 0x91 DIR1        ; /* SFR specif to 71M654x */
__sfr __at 0x92 DPS         ;
__sfr __at 0x94 ERASE       ; /* SFR specif to 71M654x */
__sfr __at 0x98 S0CON       ;
__sfr __at 0x99 S0BUF       ;
__sfr __at 0x9A IEN2        ;
__sfr __at 0x9B S1CON       ;
__sfr __at 0x9C S1BUF       ;
__sfr __at 0x9D S1RELL      ;
__sfr __at 0x9E EEDATA      ; /* SFR specif to 71M654x */
__sfr __at 0x9F EECTRL      ; /* SFR specif to 71M654x */
__sfr __at 0xA0 P2          ;
__sfr __at 0xA1 DIR2        ; /* SFR specif to 71M654x */
__sfr __at 0xA2 DIR0        ; /* SFR specif to 71M654x */
__sfr __at 0xA8 IE          ;
__sfr __at 0xA8 IEN0        ; /* as called by Siemens */
__sfr __at 0xA9 IP0         ; /* interrupt priority register - SAB80515 specific */
__sfr __at 0xAA S0RELL      ;
__sfr __at 0xB0 P3          ;
__sfr __at 0xB2 FLSHCTL     ; /* SFR specif to 71M654x */
__sfr __at 0xB6 FL_BANK     ; /* SFR specif to 71M654x */
__sfr __at 0xB7 PGADR       ; /* SFR specif to 71M654x */
__sfr __at 0xB8 IEN1        ; /* interrupt enable register - SAB80515 specific */
__sfr __at 0xB9 IP1         ; /* interrupt priority register as called by Siemens */
__sfr __at 0xBA S0RELH      ;
__sfr __at 0xBB S1RELH      ;
__sfr __at 0xBF PDATA       ;
__sfr __at 0xBF USR2        ;
__sfr __at 0xBF PDATA       ; /* as called by Teridian */
__sfr __at 0xC0 IRCON       ; /* interrupt control register - SAB80515 specific */
__sfr __at 0xC8 T2CON       ;
__sfr __at 0xD0 PSW         ;
__sfr __at 0xD8 WDCON       ; /* Baud Rate Control Register  */
__sfr __at 0xE0 ACC         ;
__sfr __at 0xE0 A           ;
__sfr __at 0xE8 FLAG0		    ; 
__sfr __at 0xE8 IFLAGS	    ; /* as called by Teridian */
__sfr __at 0xF0 B           ;
__sfr __at 0xF8 FLAG1		    ;
__sfr __at 0xF9 VSTAT		    ; /* SFR specif to 71M654x */
__sfr __at 0xFC REMOTE0	    ; /* SFR specif to 71M654x */
__sfr __at 0xFD SPI1		    ; /* SFR specif to 71M654x */


/* Variaveis de teste para DEBUG */
__sfr __at 0xFF TESTE0		;
__sfr __at 0xFE TESTE1		;
__sfr __at 0xFD TESTE2		;
__sfr __at 0xFC TESTE3		;
__sfr __at 0xFB TESTE4		;

__xdata __at (0x3500) volatile unsigned char TESTE5;
__xdata __at (0x3501) volatile unsigned char TESTE6;
__xdata __at (0x3502) volatile unsigned char TESTE7;
__xdata __at (0x3503) volatile unsigned char TESTE8;
__xdata __at (0x3504) volatile unsigned char TESTE9;
__xdata __at (0x3505) volatile unsigned char RFLAG;

__xdata __at (0x3508) volatile unsigned short VAR1;
__xdata __at (0x350A) volatile unsigned short VAR2;
__xdata __at (0x350C) volatile unsigned short VAR3;
__xdata __at (0x3510) volatile unsigned char ERROR;
__xdata __at (0x3511) volatile unsigned char STEP;

/* BIT addressable registers */
/* P0 */
__sbit __at 0x80 P0_0       ;
__sbit __at 0x81 P0_1       ;
__sbit __at 0x82 P0_2       ;
__sbit __at 0x83 P0_3       ;
__sbit __at 0x94 P0_IO_0    ;
__sbit __at 0x95 P0_IO_1    ;
__sbit __at 0x96 P0_IO_2    ;
__sbit __at 0x97 P0_IO_3    ;

/* TCON */
__sbit __at 0x88 IT0        ;
__sbit __at 0x89 IE0        ;
__sbit __at 0x8A IT1        ;
__sbit __at 0x8B IE1        ;
__sbit __at 0x8C TR0        ;
__sbit __at 0x8D TF0        ;
__sbit __at 0x8E TR1        ;
__sbit __at 0x8F TF1        ;

/* P1 */
__sbit __at 0x90 P1_0       ;
__sbit __at 0x91 P1_1       ;
__sbit __at 0x92 P1_2       ;
__sbit __at 0x93 P1_3       ;
__sbit __at 0x94 P1_IO_0    ;
__sbit __at 0x95 P1_IO_1    ;
__sbit __at 0x96 P1_IO_2    ;
__sbit __at 0x97 P1_IO_3    ;

//A Confirmar - TODO
__sbit __at 0x90 INT3_CC0   ; /* P1 alternate functions - SAB80515 specific */
__sbit __at 0x91 INT4_CC1   ;
__sbit __at 0x92 INT5_CC2   ;
__sbit __at 0x93 INT6_CC3   ;
__sbit __at 0x94 INT2       ;
__sbit __at 0x95 T2EX       ;
__sbit __at 0x96 CLKOUT     ;
__sbit __at 0x97 T2         ;

/* S0CON */
__sbit __at 0x98 RI0        ;
__sbit __at 0x99 TI0        ;
__sbit __at 0x9A RB80       ;
__sbit __at 0x9B TB80       ;
__sbit __at 0x9C REN0       ;
__sbit __at 0x9D SM20       ;
__sbit __at 0x9E SM1        ;
__sbit __at 0x9F SM0        ;

/* S1CON */
__sbit __at 0x9B RI1        ;
__sbit __at 0x9C TI1        ;
__sbit __at 0x9D RB81       ;
__sbit __at 0x9E TB81       ;
__sbit __at 0x9F REN1       ;
__sbit __at 0xA0 SM21       ;
__sbit __at 0xA1 SM         ;

/* P2 */
__sbit __at 0xA0 P2_0       ;
__sbit __at 0xA1 P2_1       ;
__sbit __at 0xA2 P2_2       ;
__sbit __at 0xA3 P2_3       ;
__sbit __at 0xA4 P2_IO_0    ;
__sbit __at 0xA5 P2_IO_1    ;
__sbit __at 0xA6 P2_IO_2    ;
__sbit __at 0xA7 P2_IO_3    ;

/* IEN0 */
__sbit __at 0xA8 EX0        ;
__sbit __at 0xA9 ET0        ;
__sbit __at 0xAA EX1        ;
__sbit __at 0xAB ET1        ;
__sbit __at 0xAC ES         ;
//__sbit __at 0xAD ET2        ;
//__sbit __at 0xAE WDT        ; /* watchdog timer reset - SAB80515 specific */
__sbit __at 0xAF EA         ;
__sbit __at 0xAF EAL        ; /* EA as called by Siemens */

/* P3 */
__sbit __at 0xB0 P3_0       ;
__sbit __at 0xB1 P3_1       ;
__sbit __at 0xB2 P3_2       ;
__sbit __at 0xB3 P3_3       ;
__sbit __at 0xB4 P3_IO_0    ;
__sbit __at 0xB5 P3_IO_1    ;
__sbit __at 0xB6 P3_IO_2    ;
__sbit __at 0xB7 P3_IO_3    ;

//Verificar
/*__sbit __at 0xB0 RXD        ;
__sbit __at 0xB1 TXD        ;
__sbit __at 0xB2 INT0       ;
__sbit __at 0xB3 INT1       ;
__sbit __at 0xB4 T0         ;
__sbit __at 0xB5 T1         ;
__sbit __at 0xB6 WR         ;
__sbit __at 0xB7 RD         ;*/

/* IEN1 */
//__sbit __at 0xB8 EADC       ; /* A/D converter interrupt enable */
__sbit __at 0xB9 EX2        ;
__sbit __at 0xBA EX3        ;
__sbit __at 0xBB EX4        ;
__sbit __at 0xBC EX5        ;
__sbit __at 0xBD EX6        ;
//__sbit __at 0xBE SWDT       ; /* watchdog timer start/reset */
//__sbit __at 0xBF EXEN2      ; /* timer2 external reload interrupt enable */

/* IRCON */
//__sbit __at 0xC0 IADC       ; /* A/D converter irq flag */
__sbit __at 0xC1 IEX2       ; /* external interrupt edge detect flag */
__sbit __at 0xC2 IEX3       ;
__sbit __at 0xC3 IEX4       ;
__sbit __at 0xC4 IEX5       ;
__sbit __at 0xC5 IEX6       ;
//__sbit __at 0xC6 TF2        ; /* timer 2 owerflow flag  */
//__sbit __at 0xC7 EXF2       ; /* timer2 reload flag */

/* T2CON */
__sbit __at 0xC8 T2CON_0    ;
__sbit __at 0xC9 T2CON_1    ;
__sbit __at 0xCA T2CON_2    ;
__sbit __at 0xCB T2CON_3    ;
__sbit __at 0xCC T2CON_4    ;
__sbit __at 0xCD T2CON_5    ;
__sbit __at 0xCE T2CON_6    ;
__sbit __at 0xCF T2CON_7    ;

//__sbit __at 0xC8 T2I0       ;
//__sbit __at 0xC9 T2I1       ;
//__sbit __at 0xCA T2CM       ;
//__sbit __at 0xCB T2R0       ;
//__sbit __at 0xCC T2R1       ;
__sbit __at 0xCD I2FR       ;
__sbit __at 0xCE I3FR       ;
//__sbit __at 0xCF T2PS       ;


/* PSW */
__sbit __at 0xD0 P          ;
__sbit __at 0xD1 FL         ;
__sbit __at 0xD2 OV         ;
__sbit __at 0xD3 RS0        ;
__sbit __at 0xD4 RS1        ;
__sbit __at 0xD5 F0         ;
__sbit __at 0xD6 AC         ;
__sbit __at 0xD7 CY         ;
__sbit __at 0xD7 CV         ; /* De acordo com o manual da Maxim */
__sbit __at 0xD1 F1         ;

/* ADCON */
__sbit __at 0xD8 MX0        ;
__sbit __at 0xD9 MX1        ;
__sbit __at 0xDA MX2        ;
__sbit __at 0xDB ADM        ;
__sbit __at 0xDC BSY        ;

__sbit __at 0xDE CLK        ;
__sbit __at 0xDF BD         ;

/* A */
__sbit __at 0xA0 AREG_F0    ;
__sbit __at 0xA1 AREG_F1    ;
__sbit __at 0xA2 AREG_F2    ;
__sbit __at 0xA3 AREG_F3    ;
__sbit __at 0xA4 AREG_F4    ;
__sbit __at 0xA5 AREG_F5    ;
__sbit __at 0xA6 AREG_F6    ;
__sbit __at 0xA7 AREG_F7    ;

/* B */
__sbit __at 0xF0 BREG_F0    ;
__sbit __at 0xF1 BREG_F1    ;
__sbit __at 0xF2 BREG_F2    ;
__sbit __at 0xF3 BREG_F3    ;
__sbit __at 0xF4 BREG_F4    ;
__sbit __at 0xF5 BREG_F5    ;
__sbit __at 0xF6 BREG_F6    ;
__sbit __at 0xF7 BREG_F7    ;

/* BIT definitions for bits that are not directly accessible */
/* PCON bits */
#define IDL             0x01
#define PD              0x02
#define GF0             0x04
#define GF1             0x08
#define SMOD            0x80

#define IDL_            0x01
#define PD_             0x02
#define GF0_            0x04
#define GF1_            0x08
#define SMOD_           0x80

/* TMOD bits */
#define M0_0            0x01
#define M1_0            0x02
#define C_T0            0x04
#define GATE0           0x08
#define M0_1            0x10
#define M1_1            0x20
#define C_T1            0x40
#define GATE1           0x80

#define M0_0_           0x01
#define M1_0_           0x02
#define C_T0_           0x04
#define GATE0_          0x08
#define M0_1_           0x10
#define M1_1_           0x20
#define C_T1_           0x40
#define GATE1_          0x80

#define T0_M0           0x01
#define T0_M1           0x02
#define T0_CT           0x04
#define T0_GATE         0x08
#define T1_M0           0x10
#define T1_M1           0x20
#define T1_CT           0x40
#define T1_GATE         0x80

#define T0_M0_          0x01
#define T0_M1_          0x02
#define T0_CT_          0x04
#define T0_GATE_        0x08
#define T1_M0_          0x10
#define T1_M1_          0x20
#define T1_CT_          0x40
#define T1_GATE_        0x80

#define T0_MASK         0x0F
#define T1_MASK         0xF0

#define T0_MASK_        0x0F
#define T1_MASK_        0xF0

/* T2MOD bits */
#define DCEN            0x01
#define T2OE            0x02

#define DCEN_           0x01
#define T2OE_           0x02

/* WMCON bits */
#define WMCON_WDTEN     0x01
#define WMCON_WDTRST    0x02
#define WMCON_DPS       0x04
#define WMCON_EEMEN     0x08
#define WMCON_EEMWE     0x10
#define WMCON_PS0       0x20
#define WMCON_PS1       0x40
#define WMCON_PS2       0x80

/* SPCR-SPI bits */
#define SPCR_SPR0       0x01
#define SPCR_SPR1       0x02
#define SPCR_CPHA       0x04
#define SPCR_CPOL       0x08
#define SPCR_MSTR       0x10
#define SPCR_DORD       0x20
#define SPCR_SPE        0x40
#define SPCR_SPIE       0x80

/* SPSR-SPI bits */
#define SPSR_WCOL       0x40
#define SPSR_SPIF       0x80

/* SPDR-SPI bits */
#define SPDR_SPD0       0x10
#define SPDR_SPD1       0x20
#define SPDR_SPD2       0x40
#define SPDR_SPD3       0x80
#define SPDR_SPD4       0x10
#define SPDR_SPD5       0x20
#define SPDR_SPD6       0x40
#define SPDR_SPD7       0x80

/* Interrupt numbers: address = (number * 8) + 3 */
#define IE0_VECTOR      0       /* 0x03 external interrupt 0 */
#define TF0_VECTOR      1       /* 0x0b timer 0 */
#define IE1_VECTOR      2       /* 0x13 external interrupt 1 */
#define TF1_VECTOR      3       /* 0x1b timer 1 */
#define SI0_VECTOR      4       /* 0x23 serial port 0 */
#define TF2_VECTOR      5       /* 0x2B timer 2 */
#define EX2_VECTOR      5       /* 0x2B external interrupt 2 */

#define IADC_VECTOR     8       /* 0x43 A/D converter interrupt */
#define IEX2_VECTOR     9       /* 0x4B external interrupt 2 */
#define IEX3_VECTOR    10       /* 0x53 external interrupt 3 */
#define IEX4_VECTOR    11       /* 0x5B external interrupt 4 */
#define IEX5_VECTOR    12       /* 0x63 external interrupt 5 */
#define IEX6_VECTOR    13       /* 0x6B external interrupt 6 */

__xdata __at (0x0000) unsigned char CE_RESERVED;

/* I/O RAM Map – Functional Order, Basic Configuration */
volatile __xdata __at (0x28B4) unsigned char WDRST;
volatile __xdata __at (0x270C) unsigned char DIO3;
volatile __xdata __at (0x28B0) unsigned char WF1; //WF_CSTART|WF_RST|WF_RSTBIT|WF_OVF 		WF_ERST|WF_BADVDD|U|U
volatile __xdata __at (0x28B1) unsigned char WF2;

/* LCD MAP */
volatile __xdata __at (0x201A) unsigned char LCD_MAP0; //LCD_MAP[55:48]
volatile __xdata __at (0x2019) unsigned char LCD_MAP1; //LCD_MAP[47:40]
volatile __xdata __at (0x2018) unsigned char LCD_MAP2; //LCD_MAP[39:32]
volatile __xdata __at (0x2017) unsigned char LCD_MAP3; //LCD_MAP[31:24]
volatile __xdata __at (0x2016) unsigned char LCD_MAP4; //LCD_MAP[23:16]
volatile __xdata __at (0x2015) unsigned char LCD_MAP5; //LCD_MAP[15:8]
volatile __xdata __at (0x2014) unsigned char LCD_MAP6; //LCD_MAP[7:0]

/* SEGDIO */
volatile __xdata __at (0x2410) unsigned char SEGDIO0;
volatile __xdata __at (0x2411) unsigned char SEGDIO1;
volatile __xdata __at (0x2412) unsigned char SEGDIO2;
volatile __xdata __at (0x2413) unsigned char SEGDIO3;
volatile __xdata __at (0x2414) unsigned char SEGDIO4;
volatile __xdata __at (0x2415) unsigned char SEGDIO5;
volatile __xdata __at (0x2416) unsigned char SEGDIO6;
volatile __xdata __at (0x2417) unsigned char SEGDIO7;
volatile __xdata __at (0x2418) unsigned char SEGDIO8;
volatile __xdata __at (0x2419) unsigned char SEGDIO9;
volatile __xdata __at (0x241A) unsigned char SEGDIO10;
volatile __xdata __at (0x241B) unsigned char SEGDIO11;
volatile __xdata __at (0x241C) unsigned char SEGDIO12;
volatile __xdata __at (0x241D) unsigned char SEGDIO13;
volatile __xdata __at (0x241E) unsigned char SEGDIO14;
volatile __xdata __at (0x241F) unsigned char SEGDIO15;
volatile __xdata __at (0x2420) unsigned char SEGDIO16;
volatile __xdata __at (0x2421) unsigned char SEGDIO17;
volatile __xdata __at (0x2422) unsigned char SEGDIO18;
volatile __xdata __at (0x2423) unsigned char SEGDIO19;
volatile __xdata __at (0x2424) unsigned char SEGDIO20;
volatile __xdata __at (0x2425) unsigned char SEGDIO21;
volatile __xdata __at (0x2426) unsigned char SEGDIO22;
volatile __xdata __at (0x2427) unsigned char SEGDIO23;
volatile __xdata __at (0x2428) unsigned char SEGDIO24;
volatile __xdata __at (0x2429) unsigned char SEGDIO25;
volatile __xdata __at (0x242A) unsigned char SEGDIO26;
volatile __xdata __at (0x242B) unsigned char SEGDIO27;
volatile __xdata __at (0x242C) unsigned char SEGDIO28;
volatile __xdata __at (0x242D) unsigned char SEGDIO29;
volatile __xdata __at (0x242E) unsigned char SEGDIO30;
volatile __xdata __at (0x242F) unsigned char SEGDIO31;
volatile __xdata __at (0x2430) unsigned char SEGDIO32;
volatile __xdata __at (0x2431) unsigned char SEGDIO33;
volatile __xdata __at (0x2432) unsigned char SEGDIO34;
volatile __xdata __at (0x2433) unsigned char SEGDIO35;
volatile __xdata __at (0x2434) unsigned char SEGDIO36;
volatile __xdata __at (0x2435) unsigned char SEGDIO37;
volatile __xdata __at (0x2436) unsigned char SEGDIO38;
volatile __xdata __at (0x2437) unsigned char SEGDIO39;
volatile __xdata __at (0x2438) unsigned char SEGDIO40;
volatile __xdata __at (0x2439) unsigned char SEGDIO41;
volatile __xdata __at (0x243A) unsigned char SEGDIO42;
volatile __xdata __at (0x243B) unsigned char SEGDIO43;
volatile __xdata __at (0x243C) unsigned char SEGDIO44;
volatile __xdata __at (0x243D) unsigned char SEGDIO45;
volatile __xdata __at (0x243E) unsigned char SEGDIO46;
volatile __xdata __at (0x243F) unsigned char SEGDIO47;
volatile __xdata __at (0x2440) unsigned char SEGDIO48;
volatile __xdata __at (0x2441) unsigned char SEGDIO49;
volatile __xdata __at (0x2442) unsigned char SEGDIO50;
volatile __xdata __at (0x2443) unsigned char SEGDIO51;
volatile __xdata __at (0x2444) unsigned char SEGDIO52;
volatile __xdata __at (0x2445) unsigned char SEGDIO53;
volatile __xdata __at (0x2446) unsigned char SEGDIO54;
volatile __xdata __at (0x2447) unsigned char SEGDIO55;

/* Configuration CE */
volatile __xdata __at (0x2106) unsigned char CE6; //EQU[7:5] | CHOP_E[2:3] | RTM_E[1] | CE_E[0]
volatile __xdata __at (0x2107) unsigned char CE5; //SUM_SAMPS[12:8]
volatile __xdata __at (0x2108) unsigned char CE4; //SUM_SAMPS[7:0]
volatile __xdata __at (0x2109) unsigned char CE3; // = 0x39; //CE_LCTN([6:0] to 71M6543G and [5:0] to 71M6543F)
volatile __xdata __at (0x210A) unsigned char CE2;
volatile __xdata __at (0x210B) unsigned char CE1;
volatile __xdata __at (0x210C) unsigned char CE0;
volatile __xdata __at (0x210D) unsigned char RTM0_H;
volatile __xdata __at (0x210E) unsigned char RTM0;
volatile __xdata __at (0x210F) unsigned char RTM1;
volatile __xdata __at (0x2110) unsigned char RTM2;
volatile __xdata __at (0x2111) unsigned char RTM3;
volatile __xdata __at (0x2709) unsigned char RCE0;
volatile __xdata __at (0x270A) unsigned char RTMUX;
volatile __xdata __at (0x2501) unsigned char FOVRD;
volatile __xdata __at (0x2100) unsigned char MUX5;
volatile __xdata __at (0x2101) unsigned char MUX4;
volatile __xdata __at (0x2102) unsigned char MUX3;
volatile __xdata __at (0x2103) unsigned char MUX2;
volatile __xdata __at (0x2104) unsigned char MUX1;
volatile __xdata __at (0x2105) unsigned char MUX0;
volatile __xdata __at (0x28A0) unsigned char TEMP;
volatile __xdata __at (0x2400) unsigned char LCD0;
volatile __xdata __at (0x2401) unsigned char LCD1;
volatile __xdata __at (0x2200) unsigned char CKGN;

//LCD/DIO
volatile __xdata __at (0x2450) unsigned char DIO_R5;
volatile __xdata __at (0x2451) unsigned char DIO_R4;
volatile __xdata __at (0x2452) unsigned char DIO_R3;
volatile __xdata __at (0x2453) unsigned char DIO_R2;
volatile __xdata __at (0x2454) unsigned char DIO_R1;
volatile __xdata __at (0x2455) unsigned char DIO_R0;
volatile __xdata __at (0x2456) unsigned char DIO0;
volatile __xdata __at (0x2457) unsigned char DIO1;
volatile __xdata __at (0x2458) unsigned char DIO2;

//RBITS
volatile __xdata __at (0x2700) unsigned char INT1_E;
volatile __xdata __at (0x2701) unsigned char INT2_E;
volatile __xdata __at (0x2702) unsigned char SECURE;
volatile __xdata __at (0x2703) unsigned char Analog0;
volatile __xdata __at (0x2704) unsigned char VERSION;
volatile __xdata __at (0x2709) unsigned char RCE0;
volatile __xdata __at (0x270A) unsigned char RTMUX;
volatile __xdata __at (0x270C) unsigned char DIO3;


//"Atalhos, funções rápidas"
#define RESET_WD()  (WDRST = 0x80)
#define COLDSTART() (0 != (WF1 & WF_CSTART))


#endif

