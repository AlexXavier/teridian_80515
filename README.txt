Repositório para armazenar o código fonte referente ao port do FreeRTOS para o 8051 e instruções para realizar os testes.

O port foi construído com o SDCC 2.8.0 para linux, o SDCC e o método de instalação se encontram neste repositório em:
	sdcc-2.8.0-i386-unknown-linux2.5/sdcc

Para a compilação é necessário instalar o programa make e copiar o conteúdo na pasta sdcc-2.8.0-i386-unknown-linux2.5/sdcc280_lib_large-stack-auto/ para SDCC\lib\large-stack-auto\(no local onde foi instalado o SDCC)
