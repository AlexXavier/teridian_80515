# LaTeX2HTML 2002-2-1 (1.71)
# Associate internals original text with physical files.


$key = q/sub:Common-interrupt-pitfall-stack-overflow/;
$ref_files{$key} = "$dir".q|node66.html|; 
$noresave{$key} = "$nosave";

$key = q/lyx:-codeseg/;
$ref_files{$key} = "$dir".q|node54.html|; 
$noresave{$key} = "$nosave";

$key = q/lyx:-pedantic-parse-number/;
$ref_files{$key} = "$dir".q|node47.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:External-Stack/;
$ref_files{$key} = "$dir".q|node96.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:_switch_-Statements/;
$ref_files{$key} = "$dir".q|node172.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:Command-Line-Options/;
$ref_files{$key} = "$dir".q|node45.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:MCS51-variants/;
$ref_files{$key} = "$dir".q|node101.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Common-interrupt-pitfall-non-reentrant/;
$ref_files{$key} = "$dir".q|node66.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:PIC16_Header-Files/;
$ref_files{$key} = "$dir".q|node124.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Overlaying/;
$ref_files{$key} = "$dir".q|node64.html|; 
$noresave{$key} = "$nosave";

$key = q/lyx:--callee-saves-function1__function2___function3_.../;
$ref_files{$key} = "$dir".q|node54.html|; 
$noresave{$key} = "$nosave";

$key = q/ite:pedantic_parse_number/;
$ref_files{$key} = "$dir".q|node98.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Requesting-Features/;
$ref_files{$key} = "$dir".q|node155.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Windows-Install/;
$ref_files{$key} = "$dir".q|node22.html|; 
$noresave{$key} = "$nosave";

$key = q/OMF_file/;
$ref_files{$key} = "$dir".q|node40.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Interrupt-Service-Routines/;
$ref_files{$key} = "$dir".q|node65.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Install-paths/;
$ref_files{$key} = "$dir".q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/lyx:more-pedantic-SPLINT/;
$ref_files{$key} = "$dir".q|node54.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Building-SDCC-on-Linux/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:The-anatomy-of/;
$ref_files{$key} = "$dir".q|node183.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Search-Paths/;
$ref_files{$key} = "$dir".q|node13.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Functions-using-private-banks/;
$ref_files{$key} = "$dir".q|node74.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:ANSI-Compliance/;
$ref_files{$key} = "$dir".q|node179.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Loop-Optimizations/;
$ref_files{$key} = "$dir".q|node169.html|; 
$noresave{$key} = "$nosave";

$key = q/ite:callee_saves-function1__function2__function3...__--/;
$ref_files{$key} = "$dir".q|node98.html|; 
$noresave{$key} = "$nosave";

$key = q/ite:less_pedantic/;
$ref_files{$key} = "$dir".q|node98.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:Porting-code-to-other-compilers/;
$ref_files{$key} = "$dir".q|node146.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Install-Trouble-shooting/;
$ref_files{$key} = "$dir".q|node27.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Peephole-Optimizer/;
$ref_files{$key} = "$dir".q|node178.html|; 
$noresave{$key} = "$nosave";

$key = q/cha:Debugging-with-SDCDB/;
$ref_files{$key} = "$dir".q|node137.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:Compatibility-with-previous/;
$ref_files{$key} = "$dir".q|node6.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:Parameters-and-Local-Variables/;
$ref_files{$key} = "$dir".q|node63.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Naked-Functions/;
$ref_files{$key} = "$dir".q|node81.html|; 
$noresave{$key} = "$nosave";

$key = q/type_promotion/;
$ref_files{$key} = "$dir".q|node145.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:A-Step-by_Assembler_Introduction/;
$ref_files{$key} = "$dir".q|node80.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Common-interrupt-pitfall-volatile/;
$ref_files{$key} = "$dir".q|node66.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:Testing-the-SDCC/;
$ref_files{$key} = "$dir".q|node26.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Postprocessing-the-Intel/;
$ref_files{$key} = "$dir".q|node41.html|; 
$noresave{$key} = "$nosave";

$key = q/lyx:--short-is-8bits/;
$ref_files{$key} = "$dir".q|node54.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:Related-open-source-tools/;
$ref_files{$key} = "$dir".q|node149.html|; 
$noresave{$key} = "$nosave";

$key = q/lyx:-Wl_option/;
$ref_files{$key} = "$dir".q|node48.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:Quality-control/;
$ref_files{$key} = "$dir".q|node162.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Intermediate-Dump-Options/;
$ref_files{$key} = "$dir".q|node55.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Common-interrupt-pitfall-non-atomic/;
$ref_files{$key} = "$dir".q|node66.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:PIC16_Pragmas/;
$ref_files{$key} = "$dir".q|node123.html|; 
$noresave{$key} = "$nosave";

$key = q/sub:Startup-Code/;
$ref_files{$key} = "$dir".q|node75.html|; 
$noresave{$key} = "$nosave";

$key = q/lyx:--less-pedantic/;
$ref_files{$key} = "$dir".q|node54.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:Pragmas/;
$ref_files{$key} = "$dir".q|node98.html|; 
$noresave{$key} = "$nosave";

1;

