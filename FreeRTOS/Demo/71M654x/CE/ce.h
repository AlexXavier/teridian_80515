/***************************************************************************
 * Este arquivo tem como objetivo definir a imagem e dados do CE, além de
 * algumas váriasves específicas para a configuração do CE.
 * Na prática isso representea a mescla nos três seguintes arquivos:
 *    ce43b026603_ce.c
 *    ce43b026603_dat.c
 *    ce1r6k_2520_1p0.h
 **************************************************************************/
 
//**************************************************************************
//                                                                          
//  DESCRIPTION: 71M6540 POWER METER - CE Code Image File (8051 C format)           
//                                                                          
//  Necessário verificar parâmetros de acordo com a configuração
//  de nosso projeto                                                  
//                                                                          
//**************************************************************************
#ifndef CE_H
#define CE_H

// FIR length is 1 clock cycles, 288 ADC clocks
// MUX_DIV is 6, i.e. 13 A/D inputs, i.e. for a 3-phase meter
#define FS_INT 2520     // Sample frequency is 2520.62 Hz.
#define FS_FLOAT (32768.0/13.0)
#define SUMPRE (FS_INT)  // Fixed SUMPRE for reference meter.
// CE's PLL is stable after 1 second, so first valid data is after that.
#define CE_PLL_OK 2 // Accumulation intervals before CE's PLL is stable.

#define WH_PER_CE   (7.7562e-15)    // (1/100)*7.7562x10^-13*VMAX * IMAX / In8
// The Wh scale applied to VARh, as well.
#define W_PER_CE    (2.792231e-11)    // (1/100)*7.7562x10^-13*3600 
// The W scale applies to VAR, as well.
// Multiplied by 10^2 because a_max is in 0.1A units.
// Multiplied by FS_INT because it's divided by sumpre to compensate for a changed
// accumulation interval.
#define A_PER_CE_SQ (0.058031)      // ((1000/10)^2 * 6.3968x10^-13 * 3600 * FS_INT)
#define AID_PER_CE_SQ (0.085318)    // ((1000/10)^2 * 9.4045x10^-13 * 3600 * FS_INT)
#define V_PER_CE_SQ (0.085318)      // ((1000/10)^2 * 9.4045x10^-13 * 3600 * FS_INT)
#define SMALL_IRMS_THRESHOLD 121 // corresponds to 0.05A at 110.5A = Imax
#define V_PER_CE   (9.4045e-15)     // (1/100)*9.4045x10^-13*VMAX * VMAX
#define TENTH_HZ (0x001A0000L / 10)   // 13 * 2^17 / 10 = (2^32 / (2^15 / 13)) / 10.
#define TWENTIETH_HZ (0x001A0000L / 20)
#define SEVENTY_HZ  (TENTH_HZ * 700)
#define hz_freq(d) ((d + TWENTIETH_HZ) / TENTH_HZ)
#define DEG_SCALE (-114676L)
#define TEMP_NOM (547178834L)
#define LOGIC_BAD_MV (2000)         // logic may fail at 2V (2000 mV)
// Filtered VBAT
#define VBAT_CE_PER_MV (-113349L)
#define VBAT_BAD_CE (-223439528L) // 2V

//**************************************************************************
//                                                                          
//  DESCRIPTION: 71M6540 POWER METER - CE Code Image File (8051 C format)           
//                                                                          
//  AUTHOR:  BJW                                                            
//                                                                          
//  HISTORY: May 24 2010
//                                                                          
//**************************************************************************                                                             
// Código gerado a partir do arquivo: ce43b026603_ce.c                                                                          
// CE Program Image File (8051 C format)
__code __at (0xE3FE) short NumCeCode=1220;	// The number of words in the 'CeCode' array.
__code __at (0xE400) char CeCode[]={
 0x29,0x4f, 0xf1,0xf1, 0x8f,0x04, 0xf1,0xf0, 0x20,0x4d, 0x74,0x48, 0xec,0xff, 0x8c,0x48,
 0x20,0x75, 0x74,0x4c, 0xed,0xff, 0x8c,0x4c, 0x20,0x4d, 0x74,0x48, 0xec,0xff, 0x8c,0x48,
 0x20,0x75, 0x74,0x4c, 0xed,0xff, 0x8c,0x4c, 0x20,0x4d, 0x74,0x48, 0xec,0xff, 0x8c,0x48,
 0x20,0x75, 0x74,0x4c, 0xed,0xff, 0x8c,0x4c, 0x20,0x4d, 0x74,0x48, 0xec,0xff, 0x8c,0x48,
 0x20,0x75, 0x74,0x4c, 0xed,0xff, 0x8c,0x4c, 0x20,0x4d, 0x74,0x48, 0xec,0xff, 0x8c,0x48,
 0x20,0x75, 0x74,0x4c, 0xed,0xff, 0x8c,0x4c, 0x20,0x4d, 0x74,0x48, 0xec,0xff, 0x8c,0x48,
 0x20,0x75, 0x74,0x4c, 0xed,0xff, 0x8c,0x4c, 0x20,0x1e, 0x8f,0x76, 0x20,0x76, 0x8f,0x7b,
 0x20,0x32, 0x8f,0x77, 0x20,0x34, 0x8f,0x78, 0x20,0x5b, 0x8f,0x79, 0x20,0x5c, 0x8f,0x7a,
 0x20,0x1f, 0x8f,0x7c, 0x20,0x33, 0x8f,0x7d, 0x20,0x35, 0x8f,0x7e, 0x20,0x55, 0x8f,0x57,
 0x20,0x56, 0x8f,0x58, 0x20,0x3c, 0x8f,0x3d, 0xf0,0x4d, 0x20,0x48, 0x85,0x47, 0x30,0x48,
 0x1f,0x4f, 0x86,0x47, 0xf0,0x48, 0x2f,0x4f, 0x85,0x55, 0xb6,0x55, 0x20,0x55, 0xd0,0x57,
 0x2f,0x4f, 0x00,0x46, 0x80,0x46, 0xf0,0x75, 0x20,0x4c, 0x85,0x4b, 0x30,0x4c, 0x1f,0x4f,
 0x86,0x4b, 0xf0,0x4c, 0x2f,0x4f, 0x85,0x56, 0xb6,0x56, 0x20,0x56, 0xd0,0x58, 0x2f,0x4f,
 0x00,0x4a, 0x80,0x4a, 0x20,0xbc, 0x0f,0x4f, 0x87,0xbc, 0x20,0xbc, 0x84,0x83, 0xb4,0xbc,
 0x28,0x20, 0xaf,0x38, 0x26,0x38, 0x1f,0x4f, 0x8f,0x38, 0xdf,0x22, 0xf0,0x38, 0x20,0x33,
 0x8f,0x38, 0x20,0x35, 0x80,0x38, 0x20,0x1f, 0x86,0x38, 0x20,0xbb, 0xd0,0x50, 0xf0,0x38,
 0x2b,0x50, 0x8f,0x38, 0x30,0x38, 0x81,0x38, 0x30,0x38, 0x86,0x38, 0x20,0x38, 0x00,0xc0,
 0x87,0xc1, 0x8f,0xc0, 0xb7,0xc0, 0x20,0xc1, 0x00,0xc2, 0x8f,0x38, 0x87,0xc2, 0xf0,0xc2,
 0x22,0x50, 0xd0,0xc2, 0x8f,0x39, 0x30,0x39, 0x86,0x39, 0x20,0x39, 0x81,0xc2, 0x24,0x38,
 0x8f,0x39, 0x20,0xc3, 0x07,0x39, 0x17,0xc3, 0x8f,0xc3, 0x03,0x50, 0x15,0x50, 0x17,0x50,
 0x09,0x50, 0x8f,0xbe, 0x00,0xbf, 0x8f,0x38, 0x21,0x38, 0x84,0x82, 0x20,0xbe, 0x84,0xbf,
 0x00,0xc1, 0x74,0xbb, 0x88,0xbb, 0x22,0x20, 0x8f,0x38, 0xf0,0x38, 0x20,0x02, 0x00,0x02,
 0x8f,0x39, 0x00,0x39, 0x8f,0x39, 0x78,0x39, 0x78,0x39, 0x78,0x39, 0x8f,0x39, 0x78,0x39,
 0x8f,0x39, 0xc0,0x10, 0x40,0x39, 0x8f,0x38, 0xc0,0x41, 0x40,0x38, 0x8f,0x38, 0x20,0x38,
 0x10,0xed, 0x8f,0x38, 0xc0,0x12, 0x40,0x38, 0x00,0xed, 0x8f,0x38, 0x3a,0x38, 0x14,0xed,
 0x00,0xee, 0x05,0xef, 0x14,0xf0, 0x06,0xf0, 0x8f,0x39, 0x20,0x39, 0x04,0x39, 0x05,0x39,
 0x17,0x39, 0x1d,0x39, 0x8f,0x1e, 0x20,0xef, 0x8f,0xf0, 0x20,0xee, 0x8f,0xef, 0x20,0xed,
 0x8f,0xee, 0x20,0x38, 0x8f,0xed, 0x29,0x08, 0x12,0x68, 0x8f,0x38, 0x63,0x38, 0x8f,0x38,
 0x63,0x38, 0x8f,0x3a, 0x2e,0x1f, 0x00,0x69, 0x8f,0x69, 0x8b,0x68, 0x20,0xa8, 0x8f,0x38,
 0x20,0xa7, 0x8f,0xa8, 0x20,0xa4, 0x8f,0x39, 0xc0,0x11, 0x40,0x3a, 0x8f,0x3a, 0xc0,0x40,
 0x40,0x3a, 0x8f,0xa4, 0x20,0xa4, 0x10,0xa8, 0x8f,0x3a, 0x20,0x3a, 0x13,0x3a, 0x08,0x3a,
 0x00,0x39, 0x8f,0xa7, 0x35,0xa7, 0x00,0xa8, 0x05,0x38, 0x8f,0x1f, 0x22,0x20, 0x8f,0x38,
 0xf0,0x38, 0x20,0x04, 0x00,0x04, 0x8f,0x38, 0x00,0x38, 0x8f,0x38, 0x79,0x38, 0x79,0x38,
 0x79,0x38, 0x8f,0x38, 0x79,0x38, 0x8f,0x38, 0xc0,0x13, 0x40,0x38, 0x8f,0x38, 0xc0,0x42,
 0x40,0x38, 0x8f,0x38, 0x20,0x38, 0x10,0xf1, 0x8f,0x38, 0xc0,0x15, 0x40,0x38, 0x00,0xf1,
 0x8f,0x38, 0x3a,0x38, 0x14,0xf1, 0x00,0xf2, 0x05,0xf3, 0x14,0xf4, 0x06,0xf4, 0x8f,0x39,
 0x20,0x39, 0x04,0x39, 0x05,0x39, 0x17,0x39, 0x1d,0x39, 0x8f,0x32, 0x20,0xf3, 0x8f,0xf4,
 0x20,0xf2, 0x8f,0xf3, 0x20,0xf1, 0x8f,0xf2, 0x20,0x38, 0x8f,0xf1, 0x20,0xf1, 0x89,0x38,
 0x20,0xed, 0x10,0x38, 0x14,0x66, 0x8f,0x5b, 0x24,0x5b, 0x00,0x67, 0x8f,0x67, 0x8b,0x66,
 0x29,0x09, 0x12,0x6c, 0x8f,0x38, 0x63,0x38, 0x8f,0x38, 0x63,0x38, 0x8f,0x3a, 0x2e,0x33,
 0x00,0x6d, 0x8f,0x6d, 0x8b,0x6c, 0x20,0xaa, 0x8f,0x38, 0x20,0xa9, 0x8f,0xaa, 0x20,0xa5,
 0x8f,0x39, 0xc0,0x14, 0x40,0x3a, 0x8f,0x3a, 0xc0,0x40, 0x40,0x3a, 0x8f,0xa5, 0x20,0xa5,
 0x10,0xaa, 0x8f,0x3a, 0x20,0x3a, 0x12,0x3a, 0x16,0x3a, 0x09,0x3a, 0x00,0x39, 0x8f,0xa9,
 0x34,0xa9, 0x08,0xa9, 0x00,0xaa, 0x04,0x38, 0x07,0x38, 0x09,0x38, 0x8f,0x38, 0x16,0x38,
 0x09,0x38, 0x8f,0x33, 0x22,0x20, 0x8f,0x38, 0xf0,0x38, 0x20,0x06, 0x00,0x06, 0x8f,0x38,
 0x00,0x38, 0x8f,0x38, 0x7a,0x38, 0x7a,0x38, 0x7a,0x38, 0x8f,0x38, 0x7a,0x38, 0x8f,0x38,
 0xc0,0x16, 0x40,0x38, 0x8f,0x38, 0xc0,0x43, 0x40,0x38, 0x8f,0x38, 0x20,0x38, 0x10,0xf5,
 0x8f,0x38, 0xc0,0x18, 0x40,0x38, 0x00,0xf5, 0x8f,0x38, 0x3a,0x38, 0x14,0xf5, 0x00,0xf6,
 0x05,0xf7, 0x14,0xf8, 0x06,0xf8, 0x8f,0x39, 0x20,0x39, 0x04,0x39, 0x05,0x39, 0x17,0x39,
 0x1d,0x39, 0x8f,0x34, 0x20,0xf7, 0x8f,0xf8, 0x20,0xf6, 0x8f,0xf7, 0x20,0xf5, 0x8f,0xf6,
 0x20,0x38, 0x8f,0xf5, 0x14,0x6e, 0x8f,0x34, 0x24,0x34, 0x00,0x6f, 0x8f,0x6f, 0x8b,0x6e,
 0x20,0xf5, 0x10,0xf1, 0x10,0xf1, 0x8a,0x38, 0x20,0x38, 0x00,0xf1, 0x14,0x6a, 0x8f,0x5c,
 0x24,0x5c, 0x00,0x6b, 0x8f,0x6b, 0x8b,0x6a, 0x29,0x0a, 0x12,0x70, 0x8f,0x38, 0x63,0x38,
 0x8f,0x38, 0x63,0x38, 0x8f,0x3a, 0x2e,0x35, 0x00,0x71, 0x8f,0x71, 0x8b,0x70, 0x20,0xab,
 0x8f,0x38, 0x20,0xac, 0x8f,0xab, 0x20,0xae, 0x8f,0xac, 0x20,0xad, 0x8f,0xae, 0x20,0xa6,
 0x8f,0x39, 0xc0,0x17, 0x40,0x3a, 0x8f,0x3a, 0xc0,0x40, 0x40,0x3a, 0x8f,0xa6, 0x20,0xa6,
 0x10,0xae, 0x8f,0x3a, 0x21,0x3a, 0x04,0x3a, 0x06,0x3a, 0x07,0x3a, 0x19,0x3a, 0x00,0x39,
 0x8f,0xad, 0x33,0xad, 0x05,0xad, 0x08,0xad, 0x00,0xae, 0x04,0xac, 0x16,0xac, 0x05,0xab,
 0x07,0xab, 0x16,0x38, 0x8f,0x38, 0x20,0x38, 0x06,0x38, 0x08,0x38, 0x8f,0x35, 0x20,0x00,
 0x8f,0x38, 0xc0,0x19, 0x40,0x38, 0x8f,0x38, 0xc0,0x44, 0x40,0x38, 0x8f,0xe5, 0x14,0xe6,
 0x8f,0xe5, 0x24,0xe5, 0x00,0xe7, 0x8f,0xe7, 0x8b,0xe6, 0x22,0x1f, 0x10,0xaf, 0x10,0xaf,
 0x00,0xb0, 0x00,0xb0, 0x8f,0x38, 0x10,0xb0, 0x8f,0xb0, 0xc0,0x22, 0x4d,0x82, 0x8f,0x39,
 0xc0,0x39, 0x40,0x38, 0x8f,0xb8, 0x2e,0xb8, 0x00,0xb1, 0x8f,0xb1, 0x8b,0xaf, 0xbb,0xb1,
 0x22,0x33, 0x10,0xb2, 0x10,0xb2, 0x00,0xb3, 0x00,0xb3, 0x8f,0x38, 0x10,0xb3, 0x8f,0xb3,
 0xc0,0x22, 0x4d,0x82, 0x8f,0x39, 0xc0,0x39, 0x40,0x38, 0x8f,0xb9, 0x2e,0xb9, 0x00,0xb4,
 0x8f,0xb4, 0x8b,0xb2, 0xbb,0xb4, 0x22,0x35, 0x10,0xb5, 0x10,0xb5, 0x00,0xb6, 0x00,0xb6,
 0x8f,0x38, 0x10,0xb6, 0x8f,0xb6, 0xc0,0x22, 0x4d,0x82, 0x8f,0x39, 0xc0,0x39, 0x40,0x38,
 0x8f,0xba, 0x2e,0xba, 0x00,0xb7, 0x8f,0xb7, 0x8b,0xb5, 0xbb,0xb7, 0xce,0x1f, 0x44,0x5b,
 0x00,0x36, 0x17,0x36, 0x8f,0x36, 0xce,0x33, 0x44,0x5c, 0x00,0x37, 0x17,0x37, 0x8f,0x37,
 0xce,0x35, 0x44,0x34, 0x00,0x5a, 0x17,0x5a, 0x8f,0x5a, 0xce,0xb8, 0x44,0x5b, 0x00,0x5d,
 0x17,0x5d, 0x8f,0x5d, 0xce,0xb9, 0x44,0x5c, 0x00,0x5e, 0x17,0x5e, 0x8f,0x5e, 0xce,0xba,
 0x44,0x34, 0x00,0x5f, 0x17,0x5f, 0x8f,0x5f, 0x2f,0x5b, 0xaf,0x38, 0x20,0x5b, 0x10,0x38,
 0x8f,0x39, 0xcf,0x39, 0x40,0x39, 0x8f,0x3a, 0x20,0x39, 0x01,0x38, 0x8f,0x39, 0xc0,0x38,
 0x40,0x39, 0x00,0xa1, 0x00,0x26, 0x8f,0x38, 0x2e,0x38, 0xaf,0xa1, 0x00,0x3a, 0x10,0xc4,
 0x00,0x9e, 0x8f,0x38, 0x27,0x38, 0xaf,0x9e, 0x00,0xc4, 0x8f,0xc4, 0x2f,0x5c, 0xaf,0x38,
 0x20,0x5c, 0x10,0x38, 0x8f,0x39, 0xcf,0x39, 0x40,0x39, 0x8f,0x3a, 0x20,0x39, 0x01,0x38,
 0x8f,0x39, 0xc0,0x38, 0x40,0x39, 0x00,0xa2, 0x00,0x2a, 0x8f,0x38, 0x2e,0x38, 0xaf,0xa2,
 0x00,0x3a, 0x10,0xc5, 0x00,0xa0, 0x8f,0x38, 0x27,0x38, 0xaf,0xa0, 0x00,0xc5, 0x8f,0xc5,
 0x2f,0x34, 0xaf,0x38, 0x20,0x34, 0x10,0x38, 0x8f,0x39, 0xcf,0x39, 0x40,0x39, 0x8f,0x3a,
 0x20,0x39, 0x01,0x38, 0x8f,0x39, 0xc0,0x38, 0x40,0x39, 0x00,0xa3, 0x00,0x2e, 0x8f,0x38,
 0x2e,0x38, 0xaf,0xa3, 0x00,0x3a, 0x10,0xc6, 0x00,0x9f, 0x8f,0x38, 0x27,0x38, 0xaf,0x9f,
 0x00,0xc6, 0x8f,0xc6, 0x2f,0xe5, 0xaf,0x38, 0x20,0xe5, 0x10,0x38, 0x8f,0x39, 0xcf,0x39,
 0x40,0x39, 0x8f,0x3a, 0x20,0x39, 0x01,0x38, 0x8f,0x39, 0xc0,0x38, 0x40,0x39, 0x00,0xe8,
 0x00,0x31, 0x8f,0x38, 0x2e,0x38, 0xaf,0xe8, 0x00,0x3a, 0x10,0xea, 0x00,0xe9, 0x8f,0x38,
 0x27,0x38, 0xaf,0xe9, 0x00,0xea, 0x8f,0xea, 0xce,0x1f, 0x43,0x1f, 0x00,0xc7, 0x17,0xc7,
 0x8f,0xc7, 0xce,0x33, 0x43,0x33, 0x00,0xc8, 0x17,0xc8, 0x8f,0xc8, 0xce,0x35, 0x43,0x35,
 0x00,0xc9, 0x17,0xc9, 0x8f,0xc9, 0x20,0xc4, 0x00,0xca, 0x8f,0x38, 0x2a,0x38, 0xaf,0xca,
 0x00,0xd0, 0x8f,0xd0, 0x84,0x8c, 0xb4,0xd0, 0x20,0xca, 0x84,0x96, 0x20,0xc5, 0x00,0xcb,
 0x8f,0x38, 0x2a,0x38, 0xaf,0xcb, 0x00,0xd1, 0x8f,0xd1, 0x84,0x8d, 0xb4,0xd1, 0x20,0xcb,
 0x84,0x97, 0x20,0xc6, 0x00,0xcc, 0x8f,0x38, 0x2a,0x38, 0xaf,0xcc, 0x00,0xd2, 0x8f,0xd2,
 0x84,0x8e, 0xb4,0xd2, 0x20,0xcc, 0x84,0x98, 0x20,0xea, 0x00,0xeb, 0x8f,0x38, 0x2a,0x38,
 0xaf,0xeb, 0x00,0xec, 0x8f,0xec, 0x84,0x8f, 0xb4,0xec, 0x20,0xeb, 0x84,0x99, 0x20,0xc7,
 0x00,0xcd, 0x8f,0x38, 0x2d,0x38, 0xaf,0xcd, 0x00,0xd3, 0x8f,0xd3, 0x84,0x90, 0xb4,0xd3,
 0x20,0xc8, 0x00,0xce, 0x8f,0x38, 0x2d,0x38, 0xaf,0xce, 0x00,0xd4, 0x8f,0xd4, 0x84,0x91,
 0xb4,0xd4, 0x20,0xc9, 0x00,0xcf, 0x8f,0x38, 0x2d,0x38, 0xaf,0xcf, 0x00,0xd5, 0x8f,0xd5,
 0x84,0x92, 0xb4,0xd5, 0x60,0x36, 0x70,0x27, 0x00,0x7f, 0x8f,0x38, 0x2d,0x38, 0xaf,0x7f,
 0x00,0x60, 0x8f,0x60, 0x84,0x85, 0xb4,0x60, 0x60,0x5d, 0x70,0x28, 0x00,0x9c, 0x8f,0x38,
 0x2d,0x38, 0xaf,0x9c, 0x00,0x63, 0x8f,0x63, 0x84,0x89, 0xb4,0x63, 0x61,0x37, 0x71,0x2b,
 0x00,0x9a, 0x8f,0x38, 0x2d,0x38, 0xaf,0x9a, 0x00,0x61, 0x8f,0x61, 0x84,0x86, 0xb4,0x61,
 0x61,0x5e, 0x71,0x2c, 0x00,0x9d, 0x8f,0x38, 0x2d,0x38, 0xaf,0x9d, 0x00,0x64, 0x8f,0x64,
 0x84,0x8a, 0xb4,0x64, 0x62,0x5a, 0x72,0x2f, 0x00,0x9b, 0x8f,0x38, 0x2d,0x38, 0xaf,0x9b,
 0x00,0x62, 0x8f,0x62, 0x84,0x87, 0xb4,0x62, 0x62,0x5f, 0x72,0x30, 0x00,0xbd, 0x8f,0x38,
 0x2d,0x38, 0xaf,0xbd, 0x00,0x65, 0x8f,0x65, 0x84,0x8b, 0xb4,0x65, 0x20,0x85, 0x00,0x86,
 0x00,0x87, 0x84,0x84, 0x20,0x89, 0x00,0x8a, 0x00,0x8b, 0x84,0x88, 0x20,0x84, 0x8f,0x38,
 0x25,0x20, 0x8f,0x39, 0xf0,0x39, 0x20,0x38, 0x68,0x45, 0x84,0x4e, 0xf0,0x20, 0x20,0x4e,
 0x8f,0x4d, 0x00,0x72, 0x8f,0x38, 0x26,0x38, 0xaf,0x72, 0x8f,0x38, 0x20,0x4d, 0x68,0x38,
 0x8f,0x4d, 0xc0,0x21, 0x40,0x4d, 0x8f,0x4d, 0xf0,0x20, 0x20,0x4d, 0x79,0x4d, 0x8f,0x38,
 0x79,0x38, 0x8f,0x38, 0x79,0x38, 0x8f,0x38, 0x79,0x38, 0x8f,0x4d, 0x20,0x88, 0x8f,0x38,
 0x25,0x20, 0x8f,0x39, 0xf0,0x39, 0x20,0x38, 0x68,0x49, 0x84,0x73, 0x31,0x20, 0x11,0x20,
 0x00,0x20, 0xdf,0x22, 0xf0,0x20, 0x20,0x73, 0x8f,0x75, 0x00,0x74, 0x8f,0x38, 0x26,0x38,
 0xaf,0x74, 0x8f,0x38, 0x20,0x75, 0x68,0x38, 0x8f,0x75, 0xc0,0x21, 0x40,0x75, 0x8f,0x75,
 0xf0,0x20, 0x20,0x75, 0x79,0x75, 0x8f,0x38, 0x79,0x38, 0x8f,0x38, 0x79,0x38, 0x8f,0x38,
 0x79,0x38, 0x8f,0x75, 0xc0,0x1a, 0xf1,0xf3, 0x4d,0x01, 0xf1,0xf0, 0x8f,0x3e, 0x28,0x3e,
 0x8f,0x81, 0x20,0x3e, 0xc0,0x1b, 0x40,0x3e, 0x8f,0x59, 0xc3,0x3e, 0x40,0x3e, 0x8f,0x38,
 0xc0,0x1c, 0x40,0x38, 0x8f,0x1d, 0x2f,0x20, 0x8f,0x38, 0x27,0x38, 0x8f,0x38, 0xf0,0x38,
 0x28,0x59, 0x08,0x1d, 0x01,0x4f, 0x0f,0x4f, 0x8f,0x39, 0x20,0x40, 0x6c,0x39, 0x8f,0x40,
 0x20,0x1f, 0x8f,0xdd, 0x20,0xb8, 0x8f,0xde, 0x20,0x33, 0x8f,0xdf, 0x20,0x35, 0x8f,0xe0,
 0x27,0x20, 0x8f,0x39, 0x22,0x39, 0xaf,0x38, 0x20,0x38, 0xdf,0x4f, 0x20,0x33, 0x8e,0xdd,
 0x20,0xb9, 0x8e,0xde, 0x20,0x35, 0x8e,0xdf, 0x20,0x1f, 0x8e,0xe0, 0x20,0x38, 0xde,0x4f,
 0x20,0x35, 0x8e,0xdd, 0x20,0xba, 0x8e,0xde, 0x20,0x1f, 0x8e,0xdf, 0x20,0x33, 0x8e,0xe0,
 0x2f,0xdf, 0x8f,0x39, 0x2f,0xe0, 0x8f,0x38, 0xf0,0xdd, 0x3f,0x39, 0x00,0xd6, 0x85,0xd6,
 0x3f,0x38, 0x00,0xd8, 0x85,0xd8, 0xf0,0xde, 0x3f,0x39, 0x00,0xd7, 0x85,0xd7, 0x3f,0x38,
 0x00,0xd9, 0x85,0xd9, 0x20,0xd7, 0xd2,0x23, 0x20,0xd6, 0x81,0x38, 0x20,0x23, 0x10,0xd6,
 0x80,0x38, 0xc0,0x51, 0x40,0x52, 0x8f,0x3a, 0x20,0x38, 0x84,0xe3, 0x20,0xe3, 0x11,0x52,
 0xd0,0x3a, 0x21,0x52, 0x80,0xe1, 0x20,0xe3, 0x11,0x52, 0xd3,0x52, 0x20,0x52, 0x81,0xe1,
 0x20,0xe3, 0x8f,0x38, 0xd0,0xe1, 0x10,0x52, 0x80,0x38, 0x20,0xdb, 0x00,0x38, 0x8f,0x38,
 0xf0,0x38, 0xc0,0x52, 0x42,0x50, 0x8f,0x39, 0xc0,0x53, 0x40,0x39, 0x8f,0x39, 0x00,0x38,
 0x86,0x38, 0x20,0x38, 0x84,0xdb, 0xb4,0xd6, 0xb4,0xd7, 0x20,0xd9, 0xd2,0x23, 0x20,0xd8,
 0x81,0x38, 0x20,0x23, 0x10,0xd8, 0x80,0x38, 0x20,0x38, 0x84,0xe4, 0x20,0xe4, 0x11,0x52,
 0xd0,0x3a, 0x21,0x52, 0x80,0xe2, 0x20,0xe4, 0x11,0x52, 0xd3,0x52, 0x20,0x52, 0x81,0xe2,
 0x20,0xe4, 0xd0,0xe2, 0x10,0x52, 0x80,0x38, 0x20,0xdc, 0x00,0x38, 0x8f,0x38, 0xf0,0x38,
 0x00,0x39, 0x86,0x38, 0x20,0x38, 0x84,0xdc, 0xb4,0xd8, 0xb4,0xd9, 0x2f,0x4f, 0x00,0xda,
 0x84,0xda, 0xd0,0x53, 0xb0,0xda, 0x20,0xdb, 0x80,0x94, 0xb0,0xdb, 0x20,0xdc, 0x80,0x95,
 0xb0,0xdc, 0x20,0x54, 0x0f,0x4f, 0x8f,0x54, 0x84,0x52, 0xb4,0x54, 0x20,0x52, 0x8f,0x23,
 0x30,0x4f, 0x8f,0x38, 0xf0,0xbb, 0x2f,0x4f, 0x8f,0x3c, 0xb5,0x3c, 0x20,0x3c, 0xd0,0x3d,
 0xbf,0x3b, 0x30,0x4f, 0x8e,0x3b, 0x2f,0x20, 0x8f,0x3a, 0x26,0x3a, 0x8f,0x3a, 0xf0,0x3a,
 0x20,0x38, 0x68,0x3b, 0x8f,0x38, 0x20,0x38, 0xee,0xff, 0x8c,0x38, 0x20,0x38, 0x20,0x38,
 0x20,0x38, 0x20,0x38, 0x20,0x38, 0x30,0x4f, 0x8f,0x38, 0x20,0x38, 0xee,0xff, 0x8c,0x38,
 0x20,0x80, 0x8f,0x39, 0x23,0x39, 0xaf,0x39, 0x20,0x39, 0xf1,0xf1, 0x8f,0x11, 0xf1,0xf0,
 0x26,0x20, 0x8f,0x3a, 0xf0,0x3a, 0x2f,0x4f, 0x8f,0x39, 0x78,0x39, 0x79,0x39, 0x79,0x39,
 0x79,0x39, 0x8f,0x39, 0x8f,0xf9, 0xf1,0xf1, 0x22,0x11, 0xaf,0x14, 0xf1,0xf0, 0x02,0xf9,
 0xaf,0xfb, 0x8f,0x39, 0xf1,0xf1, 0x21,0x14, 0xaf,0x13, 0xf1,0xf0, 0x01,0xfb, 0xaf,0xfa,
 0x8f,0x3a, 0xf1,0xf1, 0x20,0x13, 0xf1,0xf0, 0x00,0xfa, 0x8f,0x3b, 0x21,0x39, 0x8f,0x39,
 0x21,0x3a, 0x8f,0x3a, 0x21,0x3b, 0x8f,0x3b, 0x20,0x39, 0x00,0x39, 0x00,0x39, 0x00,0x39,
 0x00,0x3a, 0x00,0x3a, 0x00,0x3b, 0xd0,0xf9, 0x30,0x4f, 0x8f,0x38, 0x8f,0x39, 0xbe,0x39,
 0x20,0xf9, 0xdf,0x22, 0x30,0x4f, 0x8e,0x39, 0x2f,0x20, 0x8f,0x3a, 0x25,0x3a, 0x8f,0x3a,
 0xf0,0x3a, 0x20,0x38, 0x68,0x39, 0x8f,0x38, 0x20,0x38, 0xef,0xff, 0x8c,0x38, 0xbf,0x3a,
 0x28,0x20, 0x8f,0x38, 0x2c,0x38, 0xaf,0x38, 0x20,0xfc, 0x0f,0x4f, 0x8f,0xfc, 0x20,0x1f,
 0xd0,0x24, 0xb0,0xfc, 0x20,0xfc, 0xd0,0x38, 0x20,0x3a, 0x0f,0x4f, 0x80,0x3a, 0x20,0xfd,
 0x0f,0x4f, 0x8f,0xfd, 0x20,0x33, 0xd0,0x24, 0xb0,0xfd, 0x20,0xfd, 0xd0,0x38, 0x20,0x3a,
 0x0e,0x4f, 0x80,0x3a, 0x20,0xfe, 0x0f,0x4f, 0x8f,0xfe, 0x20,0x35, 0xd0,0x24, 0xb0,0xfe,
 0x20,0xfe, 0xd0,0x38, 0x20,0x3a, 0x0d,0x4f, 0x80,0x3a, 0xf0,0xbb, 0x2c,0x4f, 0x00,0x3a,
 0x85,0x3a, 0x20,0x3a, 0x8f,0x80, 0xff,0xff,
};

//**************************************************************************
//                                                                          
//  DESCRIPTION: 71M6540 POWER METER - CE Data Image File (8051 C format)             
//                                                                          
//  AUTHOR:  BJW                                                            
//                                                                          
//  HISTORY: May 24 2010
//                                                                          
//**************************************************************************
// Código gerado a partir do arquivo: ce43b026603_dat.c                                                                       
// CE Data Image File (8051 C format)
__code __at (0xF6FE) short NumCeData=320;	// The number of words in the 'CeData' array.
__code __at (0xF700) char CeData[]={
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0xff,0xff,0xff,0xff, 0xff,0xff,0xff,0xff, 0xff,0xff,0xff,0xff,
 0x00,0x00,0x40,0x00, 0x00,0x00,0x40,0x00, 0x00,0x00,0x40,0x00, 0x00,0x00,0x40,0x00,
 0x00,0x00,0x40,0x00, 0x00,0x00,0x40,0x00, 0x00,0x00,0x40,0x00, 0x00,0x00,0x40,0x00,
 0x00,0x00,0x40,0x00, 0x00,0x00,0x40,0x00, 0x00,0x00,0xe4,0x7b, 0x00,0x00,0x00,0x2d,
 0xff,0xff,0xfd,0xb4, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x70,0x64,0x01, 0x00,0x00,0x12,0xb2, 0x00,0x00,0x19,0x2c, 0x00,0x00,0x09,0xd8,
 0x02,0xda,0x49,0x20, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x43,0x45,0x34,0x33, 0x62,0x30,0x32,0x36, 0x36,0x30,0x33,0x62, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0xff,0xff,0xff,0xff,
 0x00,0x00,0x40,0x00, 0x00,0x00,0x40,0x00, 0x00,0x00,0x40,0x00, 0x00,0x00,0x40,0x00,
 0x00,0x00,0x40,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x0c,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x0c,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x80,0x00,
 0x40,0x00,0x00,0x00, 0x00,0x00,0x18,0x00, 0x00,0x00,0x09,0xd8, 0x00,0x00,0x00,0x01,
 0xff,0xff,0xff,0xff, 0x00,0x00,0x00,0x01, 0x00,0x00,0x00,0x01, 0x00,0x00,0x00,0x01,
 0x00,0x00,0x00,0x01, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00
};

/* Internal IO RAM table; This is a demo that assumes
 * hard reset in order to show only bits the CE needs.
 * Production code should set every bit in the IC.
 * The comments describe the fields that follow. 
 * I/O Map, este são as variáveis padrões, passar manualmente, ou copiar a função: * 
 * // Moves data from a table into xdata RAM.
void xdata_table(uint8r_t *c_ptr)
{
    int16_t cnt;           // This is a count and temporary variable.
    uint8_t xdata * x_ptr; // This is a pointer to xdata RAM.
    do
    {
        // Get the pointer from the table.
        // Use "cnt" as a temporary variable.
        cnt = (int16_t)*c_ptr++;
        cnt = (cnt << 8) | (int16_t)*c_ptr++;
        if (0 == cnt) break;  // Zero marks the end of the table.
        x_ptr = (uint8_t xdata *)cnt;

        // Get the size of the data from the table.
        cnt = (int16_t)*c_ptr++;
        cnt = (cnt << 8) | (int16_t)*c_ptr++;

        // Copy some data from the table into the I/O-RAM.
        for (; 0 < cnt; --cnt)
        {
            *x_ptr++ = *c_ptr++;
        }
    }
    while (TRUE);
}

 * 
 * 
 * 
const uint8r_t io_ram_table[] =
{
   0x21, 0x00, 0x00, 0x13, // address and length
   // This is a 3-phase meter with neutral current.  
   // MUX_DIV is off at first! Changing the selectors with the multiplexor
   //   running can cause contention. 
   // Order of multiplexer is:
   //   IA, VA, IB, VB, IC, VC, IN, unused, unused, unused, unused
   //   The unused ADC slots are set to an invalid number (1) as a defensive
   //   programming practice.  1 is invalid because IA is differential,
   //   so current input 1 is IAN, thus not selectable.
   //   Remote current sensors need a different sequence:
   //   The voltages are clustered at the end of the cycle.
   //   The ADC slots at the start of the cycle should be unused, invalid
   //   ADC slot numbers (e.g. "1" if IA is differential.)
   // The equation is "5" so the CE code makes Wh = VA*IA + VB*IB + VC*IC
   // The Vref chop is automatic (the best.)
   // RTM_E is off, because we don't need ADC data out of the IC. (RTM is
   //   a bit-serial data stream from TMUX1's 0x1F selection. It sends
   //   DSP data.)
   // CE_E (the CE) is off because its input system is not yet configured.
   // SUMSAMPS is set to 0x0888, 2184 decimal, the number of mux
   //   frames per summation interval. It makes the  summation interval
   //   ~1 S. Why? ADC's FIR length = 288, 2 crystal times * 7 samples 
   //   per mux frame, and one  crystal time of silence.
   //   (2*7)+1 = (15/32768S)/sample.  So, the Fs (sample frequency) = 
   //   32768/15 = 2184.53Hz
   // The CE's code is at 0x03*0x400, 0xC00 because the Keil linker
   //   is commanded to put it there in the build options.
   // The pulse width is 0.01S, very good for most calibrators.
   // The current inputs are all differential.
   // RTM is set to the first 4 ADC locations, so if it is enabled, good
   //   data comes out.
   0x01, 0x11, 0x10, 0xA6, 0x94, 0x82, 0xA0, 0x08, // 2100..2107
   0x88, 0x03, 0x42, 0x5D, 0xF2, 0x00, 0x00, 0x01, // 2008..200F
   0x02, 0x03, 0x04,                               // 2110..2112

   0x22, 0x00, 0x00, 0x01, // address and length
   0x10,                   // Clock tree: MCK=19.6608MHz, ADC,MPU=4.9152MHz

   0x21, 0x00, 0x00, 0x01, // address and length ; set mux_div after ADC clock
   0x71,                   // Read 7 voltages per mux frame.

   0x24, 0x50, 0x00, 0x09, // address and length
   // Don't attach any interrupts or timer gates to pins.
   // Set the EEPROM interface to I2C (not needed by the CE)
   // Set the optical pin to send a Wh pulse.
   // The optical pin is not inverted. (i.e. negative-going pulse, for an LED)
   // The pulse outputs are disabled to prevent bad pulses
   //     from the CE start-up.
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x48, 0x00,  // External interrupts
   0x00, 

   0x27, 0x00, 0x00, 0x05, // address and length
   // CE interrupts are enabled, others disabled.
   // The flash controls are zeroed (not needed by the CE).
   // Vref is placed on a pin so it can be measured easily. (VREF_CAL = 1)
   //   Very helpful for debugging the temperature compensation of Vref.  
   //   Not recommended for production meters.
   // The preamp is disabled (PRE_E = 0)
   // The ADC is enabled (ADC_E = 1)
   // The battery-measurement current source is off. (not needed by the CE)
   0x61, 0x00, 0x00, 0x00, 0x90,                    // 2700, No preamp, Ch. 1

   
   0x27, 0x09, 0x00, 0x04, // address and length
   // Remote current sensors are disabled.
   // DIO outputs are enabled (PORT_E = 1)
   // Clock is sent to both the SPI and EEPROM (SPI_E = 1; for realism)
   // Slave SPI is prevented from corrupt RAM, especially CE RAM (SPI_SAFE = 1)
   0x85, 0x55, 0x00, 0x38,                    // 2709; Remotes disabled

   0x00, 0x00  // end of table
}; */

#endif //CE_H
