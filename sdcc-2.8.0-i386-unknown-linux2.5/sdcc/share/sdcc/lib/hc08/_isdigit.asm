;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 2.8.0 #5117 (Mar 23 2008) (UNIX)
; This file was generated Sun Mar 23 07:09:55 2008
;--------------------------------------------------------
	.module _isdigit
	.optsdcc -mhc08
	
	.area HOME (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT (CODE)
	.area GSFINAL (CODE)
	.area CSEG (CODE)
	.area XINIT
	.area CONST   (CODE)
	.area DSEG
	.area OSEG    (OVR)
	.area BSEG
	.area XSEG
	.area XISEG
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _isdigit
;--------------------------------------------------------
;  ram data
;--------------------------------------------------------
	.area DSEG
;--------------------------------------------------------
; overlayable items in  ram 
;--------------------------------------------------------
	.area OSEG    (OVR)
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG
;--------------------------------------------------------
; extended address mode data
;--------------------------------------------------------
	.area XSEG
_isdigit_c_1_1:
	.ds 1
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME (CODE)
	.area GSINIT (CODE)
	.area GSFINAL (CODE)
	.area GSINIT (CODE)
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME (CODE)
	.area HOME (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'isdigit'
;------------------------------------------------------------
;c                         Allocated with name '_isdigit_c_1_1'
;------------------------------------------------------------
;/home/sdcc-builder/build/sdcc-build/orig/sdcc/device/lib/_isdigit.c:26: char isdigit( unsigned char c) 
;	-----------------------------------------
;	 function isdigit
;	-----------------------------------------
_isdigit:
;/home/sdcc-builder/build/sdcc-build/orig/sdcc/device/lib/_isdigit.c:29: if ( c >= '0' && c <= '9' )   
	sta	_isdigit_c_1_1
	cmp	#0x30
	bcs	00102$
00108$:
	lda	_isdigit_c_1_1
	cmp	#0x39
	bhi	00102$
00109$:
;/home/sdcc-builder/build/sdcc-build/orig/sdcc/device/lib/_isdigit.c:30: return 1;
	lda	#0x01
	rts
00102$:
;/home/sdcc-builder/build/sdcc-build/orig/sdcc/device/lib/_isdigit.c:31: return 0;
	clra
00104$:
	rts
	.area CSEG (CODE)
	.area CONST   (CODE)
	.area XINIT
	.area CABS    (ABS,CODE)
