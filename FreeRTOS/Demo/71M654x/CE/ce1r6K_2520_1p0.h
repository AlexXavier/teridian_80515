/***************************************************************************
 * This code and information is provided "as is" without warranty of any
 * kind, either expressed or implied, including but not limited to the
 * implied warranties of merchantability and/or fitness for a particular
 * purpose.
 *
 * Copyright (C) 2009 Maxim Integrated Products Inc. All Rights Reserved.
 * DESCRIPTION: 71M65xx POWER METER - CE code constants.
 *
 * HISTORY: Necessário verificar parâmetros de acror com a configuração
 *  de nosso projeto
 **************************************************************************/
// FIR length is 1 clock cycles, 288 ADC clocks
// MUX_DIV is 6, i.e. 13 A/D inputs, i.e. for a 3-phase meter
#define FS_INT 2520     // Sample frequency is 2520.62 Hz.
#define FS_FLOAT (32768.0/13.0)
#define SUMPRE (FS_INT)  // Fixed SUMPRE for reference meter.
// CE's PLL is stable after 1 second, so first valid data is after that.
#define CE_PLL_OK 2 // Accumulation intervals before CE's PLL is stable.

#define WH_PER_CE   (7.7562e-15)    // (1/100)*7.7562x10^-13*VMAX * IMAX / In8
// The Wh scale applied to VARh, as well.
#define W_PER_CE    (2.792231e-11)    // (1/100)*7.7562x10^-13*3600 
// The W scale applies to VAR, as well.
// Multiplied by 10^2 because a_max is in 0.1A units.
// Multiplied by FS_INT because it's divided by sumpre to compensate for a changed
// accumulation interval.
#define A_PER_CE_SQ (0.058031)      // ((1000/10)^2 * 6.3968x10^-13 * 3600 * FS_INT)
#define AID_PER_CE_SQ (0.085318)    // ((1000/10)^2 * 9.4045x10^-13 * 3600 * FS_INT)
#define V_PER_CE_SQ (0.085318)      // ((1000/10)^2 * 9.4045x10^-13 * 3600 * FS_INT)
#define SMALL_IRMS_THRESHOLD 121 // corresponds to 0.05A at 110.5A = Imax
#define V_PER_CE   (9.4045e-15)     // (1/100)*9.4045x10^-13*VMAX * VMAX
#define TENTH_HZ (0x001A0000L / 10)   // 13 * 2^17 / 10 = (2^32 / (2^15 / 13)) / 10.
#define TWENTIETH_HZ (0x001A0000L / 20)
#define SEVENTY_HZ  (TENTH_HZ * 700)
#define hz_freq(d) ((d + TWENTIETH_HZ) / TENTH_HZ)
#define DEG_SCALE (-114676L)
#define TEMP_NOM (547178834L)
#define LOGIC_BAD_MV (2000)         // logic may fail at 2V (2000 mV)
// Filtered VBAT
#define VBAT_CE_PER_MV (-113349L)
#define VBAT_BAD_CE (-223439528L) // 2V

/***************************************************************************
 * History:
 * $Log: ce1r6K_2520_1p0.h,v $
 * Revision 1.3  2010/06/16 03:49:38  Ray.Vandewalker
 * linted
 *
 * Revision 1.2  2010/05/25 00:42:11  Ray.Vandewalker
 * *** empty log message ***
 *
 * Revision 1.1  2009/10/02 19:07:13  tvander
 * *** empty log message ***
 *
 * Revision 1.5  2009/07/26 00:11:52  tvander
 * Adjusted the small current threshold to 50% more than the noise floor for the preamp case.
 *
 * Revision 1.4  2009/06/24 00:34:57  tvander
 * *** empty log message ***
 *
 * Revision 1.3  2009/06/24 00:27:33  tvander
 * Constants OK.
 *
 * Revision 1.2  2009/06/23 02:37:51  tvander
 * First clean build.
 *
 * Revision 1.1  2009/01/20 19:58:37  tvander
 * *** empty log message ***
 *
 *
 * 2009 Jan 14; First Version. 
 * Copyright (C) 2009 Maxim Integrated Products Inc. All Rights Reserved.  *
 * this program is fully protected by the United States copyright          *
 * laws and is the property of Maxim Integrated Products Inc.              *
 ***************************************************************************/

